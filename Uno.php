<?php

$carte = array();
$carteUmano = array();
$carteComputer = array();
$cartaSotto = " ";
$indicatoreCartaSpeciale = 0;

//creazione mazzo

$colori = array('v','r','b','g');
$i = 0;

foreach($colori as $colore) {
       for($numero = 1; $numero<20; $numero++) {
              $carte[$i] = $numero %10 . $colore;
              $i++;
          }
    for($contatore = 0; $contatore < 2; $contatore++) {
                $carte[$i] = "p".$colore;
                $i++;
                $carte[$i] = "s".$colore;
                $i++;
                $carte[$i] = "i".$colore;
                $i++;

            }
}

for($contatore = 0; $contatore < 4; $contatore++) {
      $carte[$i] = "j"."4";
       $i++;
        $carte[$i] = "j"."0";
        $i++;
    }


//mischiare le carte
shuffle($carte);

//stampa di prova
/*for($contatore = 0; $contatore < sizeof($carte); $contatore++) {
    echo $carte[$contatore]."\n";
}
*/

echo "carte totali= ".sizeof($carte)."\n";
//distribuzione carte
distribuisciCarte($carteUmano,$carteComputer,$carte);
echo "carte dopo distribuzione= ".sizeof($carte)."\n";


//carte giù
$cartaSotto = array_shift($carte);
while((int)(substr($cartaSotto,0,1)) == '/[0-9]/'){
        array_push($carte,$cartaSotto);
       shuffle($carte);
        echo $cartaSotto;
    $cartaSotto =array_shift($carte);
}

echo "carta sotto= ".$cartaSotto;
echo "carte dopo carta sotto= ".sizeof($carte)."\n";

//funzione che scarta la carta presa in input se la mossa è corretta, altrimenti ritorna false
function scarta(&$carta)
{
       GLOBAL $cartaSotto;
    GLOBAL $indicatoreCartaSpeciale;

    //confronto se è numero uguale
    if ((substr($carta, 0, 1) == substr($cartaSotto, 0, 1))) {
                $cartaSotto = $carta;
                return true;
    }else {
            //confronto se è colore uguale
            if (substr($carta, 1, 1) == substr($cartaSotto, 1, 1)) {
                    $cartaSotto = $carta;
                    if(substr($cartaSotto, 0, 1) == "p" || substr($cartaSotto, 0, 1) == "i")
                         $indicatoreCartaSpeciale = 1;
        return true;
    }else{

                if(substr($cartaSotto,0,1) == "j") {
                       $cartaSotto = $carta;
                       $indicatoreCartaSpeciale = 1;
                       return true;
    } }}

    return false;
}


function turnoGiocatore() {
        GLOBAL $carte;
        GLOBAL $carteUmano;
        GLOBAL $cartaSotto;
        GLOBAL $carteComputer;
        GLOBAL $indicatoreCartaSpeciale;
    echo "Carta sotto: " . $cartaSotto . "\n";
    
    
    echo "Carte Giocatore\n";
    for($contatore = 0; $contatore < sizeof($carteUmano); $contatore++) {
        echo $carteUmano[$contatore]."\n";
    }

    
     if(count($carte) == 0) {
        echo "Houston abbiamo un problema";
    }
    
    //vedo la carta sotto e se � una carta speciale eseguo l'azione e salto il turno

    echo $indicatoreCartaSpeciale . "TurnoGiocatore";
    
    if($indicatoreCartaSpeciale == 1) { //se la cartasotto � appena stata scartata
        //gestione inverti e salta
        if(substr($cartaSotto,0,1) === "i" || substr($cartaSotto,0,1) === "s") {
            echo "\n inverti salta \n";
            $indicatoreCartaSpeciale = 0;
           return 0;
        }
    
        //gestione del pesca 4
        if($cartaSotto === "j4") {
           array_push($carteUmano,array_shift($carte));
           array_push($carteUmano,array_shift($carte));
           array_push($carteUmano,array_shift($carte));
           array_push($carteUmano,array_shift($carte));
           $indicatoreCartaSpeciale = 0;
          echo "\n pesca 4 carte"."\n";
          return 0;
    }

        //gestione del pesca 2
         if(substr($cartaSotto,0,1) === "p") {
           array_push($carteUmano,array_shift($carte));
          array_push($carteUmano,array_shift($carte));
          $indicatoreCartaSpeciale = 0;
           echo "\n pesca 2 carte";
          return 0;
        }
    }
    
    else{
        echo "Proseguo \n";
    }

    echo "Inserire posizione carta da giocare\n";
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    if(trim($line) < 1 || trim($line) > sizeof($carteUmano)){ //controllo che il valore sia interno al numero di carte
        $handle = fopen ("php://stdin","r");
        $line = fgets($handle);
    }
 
         

        /*$temp = $carteUmano[n-1];
        $carteUmano[n-1] = $carteUmano[$indice-1];
        $carteUmano[$indice-1] = $temp;*/

        if(scarta($carteUmano[(int)line - 1]))
        {
            foreach($cartUmano as $carta) {
              if($carta == $carteUmano[(int)line - 1]) {
                     unset($carteUmano[(int)line - 1]);
              }
              
            }
            echo "\n scarto Giocatore\n" . $carta;
            if (count($carteUmano) == 1)
                echo "UNO!";
            if (count($carteUmano) == 0) {
                echo "Ho vinto";
                exit();
            }
            return 0;
        }
        else
        {
              echo "carta non valida";
        }


    //pesca carta
    array_push($carteUmano,array_shift($carte));
    echo "\n pesco Giocatore";

    return 0;


}

function turnoPC() {
        GLOBAL $carte;
        GLOBAL $carteUmano;
        GLOBAL $cartaSotto;
        GLOBAL $carteComputer;
        GLOBAL $indicatoreCartaSpeciale;

    echo "Carta sotto: " . $cartaSotto . "\n";

    echo "Carte PC\n";
    for($contatore = 0; $contatore < sizeof($carteComputer); $contatore++) {
                echo $carteComputer[$contatore]."\n";
    }

    //vedo la carta sotto e se è una carta speciale eseguo l'azione e salto il turno

   echo $indicatoreCartaSpeciale . "TurnoPC";

   if($indicatoreCartaSpeciale == 1) {
                //gestione inverti e salta
                if(substr($cartaSotto,0,1) === "i" || substr($cartaSotto,0,1) === "s") {
                        echo "\n inverti salta \n";
            $indicatoreCartaSpeciale = 0;
           return 0;
         }



        //gestione del pesca 4
        if($cartaSotto === "j4") {
                       array_push($carteComputer,array_shift($carte));
                       array_push($carteComputer,array_shift($carte));
                       array_push($carteComputer,array_shift($carte));
                       array_push($carteComputer,array_shift($carte));
                       $indicatoreCartaSpeciale = 0;
                      echo "\n pesca 4 carte"."\n";
          return 0;
    }

        //gestione del pesca 2
         if(substr($cartaSotto,0,1) === "p") {
                       array_push($carteComputer,array_shift($carte));
                      array_push($carteComputer,array_shift($carte));
                      $indicatoreCartaSpeciale = 0;
                       echo "\n pesca 2 carte";
          return 0;
        }
    }
    else{
                echo "proseguo";
        }

    shuffle($carteComputer);
    foreach($carteComputer as $carta) {


                if(scarta($carta,$cartaSotto)) {
                        $cartaSotto = $carta;
                        echo "\n scarto PC\n".$carta;
            unset($carteComputer[array_search($carta,$carteComputer)]);
            if(count($carteComputer) == 1)
                                echo "UNO!";
            if(count($carteComputer) == 0) {
                              echo "Ho vinto";
                exit();
            }
            return 0;

        }
    }

    //pesca carta
    array_push($carteComputer,array_shift($carte));
    echo "\n pesco PC";

    return 0;

}
function distribuisciCarte() {
           GLOBAL $carte;
        GLOBAL $carteUmano;
        GLOBAL $cartaSotto;
        GLOBAL $carteComputer;

    for($i = 0; $i < 7; $i++) {

                if(count( $carte) == 0) {
                    echo "Houston abbiamo un problema";
    }
        array_push($carteUmano,array_shift($carte));
        array_push($carteComputer,array_shift($carte));
    }


}


while(count($carte) != 0) {
    turnoPC();
    turnoGiocatore();
}

?>